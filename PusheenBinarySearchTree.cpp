#include <iostream>
using namespace std;
#define NOT_A_PUSHEEN NULL
struct PUSHEEN_THE_CAT {
    int MEOW;
    PUSHEEN_THE_CAT *LEFT_PUSHEEN, *RIGHT_PUSHEEN;
} *PUSHEEN_NO_1;
void PUSHEEN_MEOW(int MEOW) {
    PUSHEEN_THE_CAT *NEW_PUSHEEN = new PUSHEEN_THE_CAT;
    NEW_PUSHEEN->MEOW = MEOW;
    NEW_PUSHEEN->LEFT_PUSHEEN = NOT_A_PUSHEEN;
    NEW_PUSHEEN->RIGHT_PUSHEEN = NOT_A_PUSHEEN;

    if (PUSHEEN_NO_1 == NOT_A_PUSHEEN) {
        PUSHEEN_NO_1 = NEW_PUSHEEN;
    } else {
        PUSHEEN_THE_CAT *PREV_PUSHEEN = PUSHEEN_NO_1;
        PUSHEEN_THE_CAT *NOW_PUSHEEN = PUSHEEN_NO_1;
        while (NOW_PUSHEEN != NOT_A_PUSHEEN) {
            PREV_PUSHEEN = NOW_PUSHEEN;
            if (NEW_PUSHEEN->MEOW > NOW_PUSHEEN->MEOW)
                NOW_PUSHEEN = NOW_PUSHEEN->RIGHT_PUSHEEN;
            else
                NOW_PUSHEEN = NOW_PUSHEEN->LEFT_PUSHEEN;
        }
        if (NEW_PUSHEEN->MEOW > PREV_PUSHEEN->MEOW)
            PREV_PUSHEEN->RIGHT_PUSHEEN = NEW_PUSHEEN;
        else
            PREV_PUSHEEN->LEFT_PUSHEEN = NEW_PUSHEEN;
    }
}

void PUSHEEN(PUSHEEN_THE_CAT *NOW_PUSHEEN) {
    if (NOW_PUSHEEN == NOT_A_PUSHEEN)
        return;
    PUSHEEN(NOW_PUSHEEN->LEFT_PUSHEEN);
    PUSHEEN(NOW_PUSHEEN->RIGHT_PUSHEEN);
    cout << NOW_PUSHEEN->MEOW << endl;
}
int main(int argc, char *argv[])
{
    int MEOW;
    while (cin >> MEOW) {
        PUSHEEN_MEOW(MEOW);
    }
    PUSHEEN(PUSHEEN_NO_1);
    return 0;
}
